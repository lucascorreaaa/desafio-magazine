/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package br.bdclasses;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author LUCAS
 */
public class AcessaBD {

    private static String URL = "jdbc:sqlserver://localhost:1433;databaseName=BDDesafio";
    private static String USUARIO = "sa";
    private static String SENHA = "lucas";
    private static String DRIVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
    ResultSet RS;
    Connection CON;
    Statement STMT;

    public void entBanco() {
        try {
            Class.forName(DRIVER);

        } catch (ClassNotFoundException e) {
            System.out.println(e.getMessage());
        }

        try {
            CON = DriverManager.getConnection(URL, USUARIO, SENHA);
            STMT = CON.createStatement();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
    }

    public boolean alterarBD(String comandoSQL) throws SQLException {
        int result = 0;
        if (STMT != null) {

            result = STMT.executeUpdate(comandoSQL);

        } else {
            System.out.println("Conexão necessita ser inicializada.");
        }
        if (result == 1) {
            return true;
        }

        return false;
    }
}
