<%-- 
    Document   : index
    Created on : 08/05/2014, 13:44:50
    Author     : LUCAS
--%>

<%@page import="br.bdclasses.AcessaBD"%>
<%@page import="br.jsonclasses.JSONObject"%>
<%@page import="br.jsonclasses.JSONString"%>
<%@page import="br.jsonclasses.JSONTokener"%>
<%@page import="br.jsonclasses.JSONArray"%>
<%@page import="java.io.InputStreamReader"%>
<%@page import="java.net.URL"%>
<%@page import="java.io.BufferedReader"%>
<%@page import="java.net.MalformedURLException"%>
<%@page import="java.io.IOException"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form name="obtencaoCep" action="index.jsp" method="POST">
            <b>Digite seu CEP: </b>
            <input type="text" name="cep" value="" size="15" maxlength="8" />
            <input type="submit" value="Submit" name="btnSubmit" />
        </form>

        <%
                String cep = request.getParameter("cep");
                try {
                    URL postmon = new URL("http://api.postmon.com.br/v1/cep/" + cep);
                    BufferedReader buff = new BufferedReader(new InputStreamReader(postmon.openStream()));
                    String txt = buff.readLine();
                    
                    JSONObject obj = new JSONObject(txt);
                    
                    String endereco = obj.getString("logradouro");
                    String bairro = obj.getString("bairro");
                    String cidade = obj.getString("cidade");
                    String estado = obj.getString("estado");
                    
                    AcessaBD acessa = new AcessaBD();
                    acessa.entBanco();
                    boolean b = acessa.alterarBD("INSERT INTO dbo.Dados (endereco, bairro, cidade, estado)" +
                    "VALUES ('"+endereco+"', '"+bairro+"', '"+cidade+"', '"+estado+"')");
                    
                    if (b == true){
                        out.print("<h3><font color=\"blue\"> Os dados de endereço foram salvos com sucesso!</font></h3>");
                    } else {
                        out.print("Erro");
                    }
                    buff.close();
                } catch (MalformedURLException excecao) {
                    excecao.printStackTrace();
                } catch (IOException excecao) {                       
                    excecao.printStackTrace();                   
           }
        %>
    </body>
</html>
