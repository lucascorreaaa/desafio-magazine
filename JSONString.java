/*
 * @author JSON.org
 * @version 2014-05-03
 */
package br.jsonclasses;


public interface JSONString {
    public String toJSONString();
}
